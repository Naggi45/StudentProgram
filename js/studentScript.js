//Start of OnLoad fn
var sidePanel = document.getElementById("opensidebar");
window.onload = function() {
    sidePanel.style.display = "none";
}
opensidebar = function() {
    sidePanel.style.display = "block";
}
hidesidebar = function() {
    sidePanel.style.display = "none";
}

//Start of createStudent fn
function createStudent() {
    var nameEle = document.getElementById("name");
    var namev = nameEle.value;
    var standardEle = document.getElementById("standard");
    var standardv = standardEle.value;
    var rollNoEle = document.getElementById("rollNo");
    var rollNov = rollNoEle.value;
    var subjectEle = document.getElementById("subject");
    var favSubjectv = subjectEle.value;
    var checkNull = false;
    if (namev === "" || namev === null) {
        checkNull = true;
        nameEle.style.border = "thick solid red";
    } else {
        nameEle.style.border = "initial";
    }
    if (standardv === "" || standardv === null) {
        checkNull = true;
        standardEle.style.border = "thick solid red";
    } else {
        standardEle.style.border = "initial";
    }
    if (rollNov === "" || rollNov === null) {
        checkNull = true;
        rollNoEle.style.border = "thick solid red";
    } else {
        rollNoEle.style.border = "initial";
    }
    var student = {
        name: namev,
        standard: standardv,
        rollNumber: rollNov,
        favSubject: favSubjectv
    };
    if (checkNull === false) {
        var httpObj = new XMLHttpRequest();
        var url = "http://localhost:8080/api/students";
        var reqObj = student;
        console.log(reqObj);
        httpObj.open('POST', url, true);
        var responseObj;
        httpObj.setRequestHeader('Content-type', 'application/json');
        httpObj.setRequestHeader("Access-Control-Allow-Origin", "*");
        httpObj.send(JSON.stringify(reqObj));

        httpObj.onreadystatechange = function() { //Call a function when the state changes.
            if (httpObj.readyState == 4 && httpObj.status == 200) {

                responseObj = JSON.parse(httpObj.responseText);
                document.getElementById("name").value = "";
                document.getElementById("standard").value = "";
                document.getElementById("rollNo").value = "";
                document.getElementById("subject").value = "Telugu";
                document.getElementById("response").innerHTML = responseObj.message;
            }
        }
    }

}

//Start of updateStudent fn
function updateStudent() {
    var nameEle = document.getElementById("name");
    var namev = nameEle.value;
    var standardEle = document.getElementById("standard");
    var standardv = standardEle.value;
    var rollNoEle = document.getElementById("rollNo");
    var rollNov = rollNoEle.value;
    var subjectEle = document.getElementById("subject");
    var favSubjectv = subjectEle.value;
    var checkNull = false;
    if (namev === "" || namev === null) {
        checkNull = true;
        nameEle.style.border = "thick solid red";
    } else {
        nameEle.style.border = "initial";
    }
    if (standardv === "" || standardv === null) {
        checkNull = true;
        standardEle.style.border = "thick solid red";
    } else {
        standardEle.style.border = "initial";
    }
    if (rollNov === "" || rollNov === null) {
        checkNull = true;
        rollNoEle.style.border = "thick solid red";
    } else {
        rollNoEle.style.border = "initial";
    }
    var student = {
        name: namev,
        standard: standardv,
        rollNumber: rollNov,
        favSubject: favSubjectv
    };
    if (checkNull === false) {
        var httpObj = new XMLHttpRequest();
        var url = "http://localhost:8080/api/students/"+rollNov;
        var reqObj = student;
        console.log(reqObj);
        httpObj.open('PUT', url, true);
        var responseObj;
        httpObj.setRequestHeader('Content-type', 'application/json');
        httpObj.setRequestHeader("Access-Control-Allow-Origin", "*");
        httpObj.send(JSON.stringify(reqObj));

        httpObj.onreadystatechange = function() { //Call a function when the state changes.
            if (httpObj.readyState == 4 && httpObj.status == 200) {
                resetFields();
                setResponseMessage(httpObj);
            }
            if (httpObj.readyState == 23 && httpObj.status == 400) {
                resetFields();
                setResponseMessage(httpObj);
            }
        }
        function resetFields(){
            document.getElementById("name").value = "";
            document.getElementById("standard").value = "";
            document.getElementById("rollNo").value = "";
            document.getElementById("subject").value = "Telugu";
        }
        function setResponseMessage(httpObj){
            responseObj = JSON.parse(httpObj.responseText);
            document.getElementById("response").innerHTML = responseObj.message;
        }
    }

}
//Start of deleteStudent fn
function deleteStudent() {
    var rollNoEle = document.getElementById("rollNo");
    var rollNov = rollNoEle.value;
    var checkNull = false;
    if (rollNov === "" || rollNov === null) {
        checkNull = true;
        rollNoEle.style.border = "thick solid red";
    } else {
        rollNoEle.style.border = "initial";
    }
    var student = {
        rollNumber: rollNov,
    };
    if (checkNull === false) {
        var httpObj = new XMLHttpRequest();
        var url = "http://localhost:8080/api/students/"+rollNov;
        var reqObj = student;
        console.log(reqObj);
        httpObj.open('DEL', url, true);
        var responseObj;
        httpObj.setRequestHeader('Content-type', 'application/json');
        httpObj.setRequestHeader("Access-Control-Allow-Origin", "*");
        httpObj.send(JSON.stringify(reqObj));

        httpObj.onreadystatechange = function() { //Call a function when the state changes.
            if (httpObj.readyState == 4 && httpObj.status == 200) {
                resetFields();
                setResponseMessage(httpObj);
            }
            if (httpObj.readyState == 23 && httpObj.status == 400) {
                resetFields();
                setResponseMessage(httpObj);
            }
        }
        function resetFields(){
            document.getElementById("rollNo").value = "";
        }
        function setResponseMessage(httpObj){
            responseObj = JSON.parse(httpObj.responseText);
            document.getElementById("response").innerHTML = responseObj.message;
        }
    }

}
//Start of deleteStudent fn
function getStudent() {
    var rollNoEle = document.getElementById("rollNo");
    var rollNov = rollNoEle.value;
    var checkNull = false;
    if (rollNov === "" || rollNov === null) {
        checkNull = true;
        rollNoEle.style.border = "thick solid red";
    } else {
        rollNoEle.style.border = "initial";
    }
    var student = {
        rollNumber: rollNov,
    };
    if (checkNull === false) {
        var httpObj = new XMLHttpRequest();
        var url = "http://localhost:8080/api/students/"+rollNov;
        var reqObj = student;
        console.log(reqObj);
        httpObj.open('GET', url, true);
        var responseObj;
        httpObj.setRequestHeader('Content-type', 'application/json');
        httpObj.setRequestHeader("Access-Control-Allow-Origin", "*");
        httpObj.send(JSON.stringify(reqObj));

        httpObj.onreadystatechange = function() { //Call a function when the state changes.
            if (httpObj.readyState == 4 && httpObj.status == 200) {
                resetFields();
                setResponseMessage(httpObj);
            }
            if (httpObj.readyState == 23 && httpObj.status == 400) {
                resetFields();
                setResponseMessage(httpObj);
            }
        }
        function resetFields(){
            document.getElementById("rollNo").value = "";
        }
        function setResponseMessage(httpObj){
            responseObj = JSON.parse(httpObj.responseText);
            document.getElementById("response").innerHTML = responseObj.message;
        }
    }

}